# Deploying a Scalable MySQL DB Cluster

We want to deploy a scalable mysql database cluster in [Digital Ocean](https://digitalocean.com) Cloud Enviornment

## Steps

1) Creating and setting up a Kubernetes Cluster in Digital Ocean 
2) Setup MySQL Cluster
3) Test various features

## Pre Requisites
1) [Kubectl CLI](https://kubernetes.io/docs/tasks/tools/)
2) [HELM CLI](https://helm.sh/)
3) Account in [Digital Ocean](https://digitalocean.com) Cloud Enviornment
4) Any MySQL Client. We will be using [MySQL Workbench](https://www.mysql.com/products/workbench/)

## Setting up Kubernetes Cluster
1) Follow the [Getting Started](https://docs.digitalocean.com/products/kubernetes/quickstart/) with kubernetes to set up our cluster. Since we have to set up MySql as scalable and make it resilient , set up min of 3 node cluster 
2) Download the kubeconfig file and keep the file in your .kube/config folder
3) Run the following command to check if connection with the cluster is successful and that you have a minimum of three clusters 
```
kubectl get nodes
```
![Kubectl Get Nodes](/Images/Get_Nodes.png)

## Setting up MySQL Database
1) Git clone the MySQL Operator by running the following command
```
git clone https://github.com/mysql/mysql-operator.git
```

![Git Clone](/Images/Clone_MySQL_Operator.png)

2) Go inside the folder with the name mysql-operator
3) Run the following helm command to install the operator in a new namespace called mysql-operator
```
helm install mysql-operator helm/mysql-operator --namespace mysql-operator --create-namespace
```

![Helm Install](/Images/Helm_Install_Operator.png)

4) Create a new namespace where we will install MySQL pods
```
kubectl create namespace mysqlcluster
```

![Kubectl create namespace](/Images/Create_Namespace.png)

5) Create a secret which will contain the password which we will use to connect to the MySQL Database
```
kubectl create secret generic mysqldbpassword --from-literal=rootUser=root --from-literal=rootHost=% --from-literal=rootPassword=<PUT YOUR PASSWORD HERE> --namespace mysqlcluster
```

![Kubectl Create Secret](/Images/Create_Secret.png)

6) Run the config.yaml file to set up MySQL Database with initial 3 instances. We can change this later as when the need arises
```
kubectl apply -f config.yaml
```
![Kubectl Apply ](/Images/Apply_Config_yaml.png)

7) Your MySQL DB is now being set up . You can watch the progress by running the following watch command

```
kubectl get innodbcluster --watch  --namespace mysqlcluster
```

![Kubectl Watch](/Images/Watch_Setup.png)
Once all pods come up , you are now ready to access your database

## Accessing the MYSQL DB
1) By default out database can only be accessed from inside the cluster. We can change this by port forwarding the service to be accessible from outside the cluster. To do this , run the following commands

```
kubectl get service mysqlcluster --namespace mysqlcluster
kubectl port-forward service/mysqlcluster mysql  --namespace mysqlcluster

```

![Kubectl Service](/Images/Port_Forwarding.png)

2) Open up MySQL Client and try connecting to localhost on port 6446 , use the password which you used while creating the secret

![Connect](/Images/Workbench_Connection.png)

![Test](/Images/Workbench_Connection_TEst.png)

3) Once connection is successful , create a database and insert some data.
```
CREATE TABLE Persons (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
lastname VARCHAR(10) NOT NULL,
firstname VARCHAR(10) NOT NULL
);

INSERT INTO Persons (firstname, lastname ) values ("Piyush", "Bhatia");

```

## Resiliance Test
1) To know which host the database connection is happening , run the following command
```
SHOW VARIABLES WHERE Variable_name = 'hostname'

```

![Get Initial Host](/Images/Hostname_Initial.png)

2) To test the resiliancy , we will delete the pod named mysqlcluster-0 and test if we are still connected to the DB and if our data is present
```
kubectl delete pod mysqlcluster-0 -n mysqlcluster
```

![Kubectl Delete Pod](/Images/DeletePod.png)

3) Test to see if data is still present
```
select * from Persons
```

![Select * ](/Images/Data.png)

4) We also see we are connected but to a different host

```
SHOW VARIABLES WHERE Variable_name = 'hostname'

```

![Get Final Host](/Images/Hostname_Final.png)

## Scaling the cluster

1) Open up config.yaml file and edit the no of instances to whaterver number is required and save file.
2) Run the below command
```
kubectl apply -f config.yaml
```
We see that the number of instances have now changed
